


ckan-pip3 install -e git+https://github.com/ckan/ckanext-harvest.git#egg=ckanext-harvest

cd $CKAN_HOME/default/src/ckanext-harvest/

ckan-pip3 install -r pip-requirements.txt

# edit

ckan.plugins = harvest ckan_harvester
ckan.harvest.mq.type = redis

#sed -i -e "s/.*search_string.*/Replacement_line/' /etc/ckan/production.ini


# check
ckan.harvest.mq.hostname (localhost)
ckan.harvest.mq.port (6379)
ckan.harvest.mq.redis_db (0)
ckan.harvest.mq.password (None)


ckan --config=/etc/ckan/production.ini harvester initdb

